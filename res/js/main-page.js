function cardHighligher(event){
  let cards = Array.from(document.querySelectorAll(".grid > a > .card"));
  let target = event.target;
  if(!cards.includes(target)){
    target = target.parentNode;
  }
  if(cards.includes(target)){
    if(localStorage.getItem("theme") == "light"){
      target.classList.add("dark-shadow");
    }else{
      target.classList.add("light-shadow");
    }
    
  }else{
    cards.forEach(element => {
      if(element.classList.contains("dark-shadow")){
        element.classList.remove("dark-shadow");
      }
      if(element.classList.contains("light-shadow")){
        element.classList.remove("light-shadow");
      }
    });
  }
}

addEventListener("load", (event) => {
  document.querySelector(".grid").addEventListener("mousemove", cardHighligher);
})