let compared = [];
let table = []; // thumb_up and thumb_down / check_circle or done and cancel or close 

function buildTable(theme, revertTheme){
  let comparison = document.createElement("div");
  comparison.classList.add("comparison");
  document.querySelector("main").append(comparison);
  let featureset = [];
  for(let app of compared){
    for(let feature of app.features){
      if(!featureset.includes(feature)){
        featureset.push(feature);
      }
    }
  }
  for(let feature of featureset){
    let results = [feature];
    for(let app of compared){
      if(app.features.includes(feature)){
        results.push(true);
      }else{
        results.push(false);
      }
    }
    table.push(results);
  }
  let grid = document.createElement("div");
  grid.classList.add("card", "grid-table", `${theme}er`);
  comparison.append(grid);

  grid.innerHTML = `
  <div class="card ${theme}">Функциональность</div>
  ${
    compared.map(app =>{
      return `
      <div class="card ${theme}">
        <img src="/res/img/apps/${app.logo}">
        <span>${app.name}</span>
      </div>
      `;
    }).join("")
  }
  ${
    table.map(
      row => {
        let name = row[0];
        let bools = row.slice(1);
        return `<div class="card ${theme}">${name}</div>` + bools.map(
          bool => `<div class="card ${theme} material-icons" style="color: ${bool ? "green" : "red"}">${bool ? "thumb_up" : "thumb_down"}</div>`
        ).join("")
      }
    ).join("")
  }
  `;

  grid.style.gridTemplateColumns = `240px repeat(${table[0].length - 1}, 100px)`;
  let totalWidth = 240 + 20 + (table[0].length - 1) * (100 + 10);
  grid.style.width = `${totalWidth}px`;
  if(totalWidth < window.innerWidth - 40){
    grid.style.position = "relative";
    grid.style.left = `${(comparison.offsetWidth - totalWidth - 20) / 2}px`;
  }
}

function clearComparison(){
  document.querySelector("main").innerHTML = "";
  table = [];
  compared = [];
  let list = getComparisonList();
  for(let slug of list){
    localStorage.removeItem(slug);
  }
  setComparisonList([]);
  setComparisonCategory(null);
  displayBasicInfo();
}

function displayBasicInfo(){
  let theme = localStorage.getItem("theme") ?? "dark";
  let revertTheme = (theme == "dark") ? "light" : "dark";

  let list = getComparisonList();
  if(list.length > 0){
    for(let slug of list){
      compared.push(
        JSON.parse(
          localStorage.getItem(slug)
        )
      );
    }
    let head = document.createElement("div");
    head.classList.add("card", theme, "content");
    head.innerHTML = `
      <h2>Сравниваем:</h2>
      ${
        compared.map(el => `<div class="card ${revertTheme}-outline" style="display: inline">${el.name}</div>`).join("")
      }
    `;
    let reset = document.createElement("div");
    reset.classList.add("card", revertTheme, "button");
    reset.addEventListener("click", clearComparison);
    reset.innerText = "Очистить сравнение"
    head.append(reset);

    document.querySelector("main").append(head);
    buildTable(theme, revertTheme);
  }else{
    let error = document.createElement("div");
    error.classList.add("card", "warning");
    error.innerText = "Вы ничего не добавили в список для сравнения!\n\
    Сначала откройте страницы различных приложений, представленных на сайте и добавьте их в список сравнения при наличии такой возможности";
    document.querySelector("main").append(error);
  }
}

addEventListener("load", event => {
  displayBasicInfo();
});