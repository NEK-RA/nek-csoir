let info;

const IMG_REPLACE_REGEX = /^<<<PUT_IMG_HERE>>>$/;

function redirectToList(){
  window.open(`${location.origin}/list/`, "_self");
}

/////////////////////////////
// Связанное со сравнением //
/////////////////////////////

function updateGoToBtn(){
  let btn = document.querySelector("#goToComparison");
  let category = getComparisonCategory();
  let list = getComparisonList();
  if(category && list.length > 0){
    btn.classList.remove("disabled");
  }else{
    btn.classList.add("disabled");
  }
}

function addOrRemoveBtnHandler(event){
  let btn = document.querySelector("#addOrRemove");
  let category = getComparisonCategory();
  if(btn.classList.contains("disabled")){
    if(category && category != info.section){
      alert("Нелья сравнивать инструменты из разных категорий!");
    }
  }else{
    let list = getComparisonList();
    index = list.indexOf(info.slug);
    if(index == -1){
      list.push(info.slug);
      if(getComparisonCategory() == null){
        setComparisonCategory(info.section);
      }
      setComparisonList(list);
      localStorage.setItem(info.slug, JSON.stringify({name: info.name, logo: info.logo, features: info.featureset}));
      btn.innerText = "Убрать из сравнения";
      updateGoToBtn();
    }else{
      list.splice(index,1);
      setComparisonList(list);
      if(list.length == 0){
        setComparisonCategory(null);
      }
      localStorage.removeItem(info.slug);
      btn.innerText = "Добавить к сравнению";
      updateGoToBtn();
    }
  }
}

function goToComparisonBtnHandler(event){
  event.preventDefault();
  if(!event.target.classList.contains("disabled")){
    window.open(`${location.origin}/compare/`, "_self");
  }
}

function generateComparisonButtons(theme, revertTheme){
  let buttons = document.createElement("div");
  buttons.classList.add("card", `${theme}er`);

  let addOrRemove = document.createElement("div");
  addOrRemove.classList.add("card", "button", `${revertTheme}`);
  addOrRemove.setAttribute("id", "addOrRemove");
  addOrRemove.addEventListener("click", addOrRemoveBtnHandler);
  
  let comparisonList = getComparisonList();
  let comparisonCategory = getComparisonCategory();
  if(!comparisonList.includes(info.slug)){
    addOrRemove.innerText = "Добавить к сравнению";
  }else{
    addOrRemove.innerText = "Убрать из сравнения";
  }
  if(comparisonCategory && comparisonCategory != info.section){
    addOrRemove.classList.add("disabled");
  }

  let goToComparison = document.createElement("div");
  goToComparison.classList.add("card", "button", `${revertTheme}`);
  goToComparison.setAttribute("id", "goToComparison");
  goToComparison.addEventListener("click", goToComparisonBtnHandler);

  goToComparison.innerText = "К сравнению"
  if (comparisonList.length == 0){
    goToComparison.classList.add("disabled");
  }

  buttons.append(addOrRemove, goToComparison);
  return buttons;
}

//////////////////////////////
// Остальная часть страницы //
//////////////////////////////

function buildHead(theme, revertTheme){
  let head = document.createElement("div");
  head.classList.add("card", "info-head", theme);

  let logoTitle = document.createElement("div");
  logoTitle.classList.add("card", "logo-title", `${theme}er`);
  logoTitle.innerHTML = `
  <img src="/res/img/apps/${info.logo}"><h2>${info.name}</h2>
  `;

  let shortInfo = document.createElement("div");
  shortInfo.classList.add("card", "short-info" ,`${theme}er`);
  shortInfo.innerHTML = `
  <span class="card">Разработчик:</span>
  <span class="card ${revertTheme}-outline">${info.developer}</span>
  
  <span class="card">Версия:</span>
  <span class="card ${revertTheme}-outline">${info.version.name}</span>

  <span class="card">Дата релиза:</span>
  <span class="card ${revertTheme}-outline">${info.version.date}</span>
  `;

  let platforms = document.createElement("div");
  platforms.classList.add("card", "platform-list", `${theme}er`);
  platforms.innerHTML = info.platforms.sort()
    .map(el => `<span class="card ${revertTheme}-outline">${el}</span>`)
    .join("")

  let link = document.createElement("div");
  link.classList.add("card", "off-link", `${theme}er`);
  link.innerHTML = `Официальный сайт:
  <a href="${info.website}" target="_blank" class="card ${revertTheme}-outline">${info.website}</a>
  `;

  let buttons = generateComparisonButtons(theme, revertTheme);

  head.append(logoTitle, shortInfo, platforms, link, buttons);
  document.querySelector("main").append(head);
}

function buildBody(theme, revertTheme){
  let cards = [];
  let images = info.images.slice();
  let contentWidth = window.innerWidth > 1000 ? 1000 : window.innerWidth;
  for(let paragraph of info.text){
    if(!paragraph.match(IMG_REPLACE_REGEX)){
      let div = document.createElement("div");
      div.classList.add("card", "paragraph", theme);
      div.innerText = paragraph;
      cards.push(div);
    }else{
      let image = document.createElement("div");
      image.classList.add("card", "picture", `${theme}er`);
      img = images.shift();
      image.innerHTML = `
      <img src="${img.url}" alt="">
      <p class="card-description">
        ${img.description}\n
      </p>
      <a href="${img.url}" target="_blank" class="card ${revertTheme}-outline"> Открыть в новой вкладке </a>
      `;
      cards.push(image);
      let pic = image.querySelector("img");
      pic.addEventListener("load", event => {
        pic.setAttribute("original-width",`${pic.offsetWidth}`);
        if((pic.offsetWidth + 50) > contentWidth){
          pic.style.width = "100%";
        }
      });
    }
  }
  document.querySelector("main").append(...cards);
}

function buildProsAndCons(theme, revertTheme){
  let pros = document.createElement("div");
  pros.classList.add("card", "pros", theme);
  pros.innerHTML = `
  <h3>Плюсы</h3>
  <ul>
  ${
    info.pros.map(p => `<li>${p}</li>`).join("")
  }
  </ul>
  `;

  let cons = document.createElement("div");
  cons.classList.add("card", "cons", theme);
  cons.innerHTML = `
  <h3>Минусы:</h3>
  <ul>
  ${
    info.cons.map(p => `<li>${p}</li>`).join("")
  }
  </ul>
  `;

  document.querySelector("main").append(pros, cons);
}

function displayInfo(){
  document.title = `${info.name} | Developer's kit`;
  let theme = localStorage.getItem("theme") ?? "dark";
  let revertTheme = (theme == "dark") ? "light" : "dark";
  buildHead(theme, revertTheme);
  buildBody(theme, revertTheme);
  buildProsAndCons(theme, revertTheme);
}

async function loadInfo(slug){
  let request = await fetch(`/res/app-info/${slug}.json`);
  if(request.status == 200){
    info = await request.json();
    displayInfo();
  }else{
    let main = document.querySelector("main");
    let errorDiv = document.createElement("div");
    errorDiv.innerText = `Произошла ошибка загрузки информации о приложении :(\nВместо корректного результата получена ошибка: ${request.status} - ${request.statusText}`;
    errorDiv.classList.add("card");
    errorDiv.classList.add("warning");
    main.append(errorDiv);
  }
}

function parseGetArguments(){
  let slug = location.search ? location.search.substring(1) : "";
  slug = slug.split("&")
    .find(param => param.startsWith("app="))
    ?.split("=")[1];
  if(slug){
    document.title = `Грузим информацию ${slug} | Developer's kit`;
    loadInfo(slug);
  }else{
    redirectToList();
  }
}

function resizeImages(event){
  let contentWidth = window.innerWidth > 1000 ? 1000 : window.innerWidth;
  document.querySelectorAll(".info-head ~ .card img")
    .forEach(pic => {
      let width = Number(pic.getAttribute("original-width"));
      if(isNaN(width)){
        width = pic.offsetWidth;
      }
      if((width + 50) > contentWidth){
        pic.style.width = "100%";
      }else{
        pic.removeAttribute("style");
      }
    });
}

addEventListener("load", event => {
  parseGetArguments();
  addEventListener("resize", resizeImages);
});