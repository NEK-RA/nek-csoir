let DEFAULT_HINT;
let FORM_FIELDS;

let EMAIL_REGEX = /^[a-z0-9]+@[a-z0-9-]+(\.[a-z0-9-]+)+$/i;

function focusHighlight(event){
  let theme = localStorage.getItem("theme") ?? "dark";
  let highlight = (theme == "dark") ? "light-shadow" : "dark-shadow";
  FORM_FIELDS.forEach(input => {
    input.classList.remove(highlight);
  });
  event.target.classList.add(highlight);
}

function unfocusInputs(event){
  if(!FORM_FIELDS.includes(event.target)){
    let theme = localStorage.getItem("theme") ?? "dark";
    let highlight = (theme == "dark") ? "light-shadow" : "dark-shadow";
    FORM_FIELDS.forEach(input => {
      input.classList.remove(highlight);
    });
  }
}

function validateForm(event){
  let messages = [];
  let hints = document.querySelector("#hints");
  let button = document.querySelector("form button");
  FORM_FIELDS.forEach(input => {
    switch(input.getAttribute("name")){
      case "name":
        if(input.value.trim().length < 2){
          messages.push("Имя должно содержать не меньше 2 символов");
          input.style.backgroundColor = "rgb(255,0,0, 0.5)";
        }else{
          input.removeAttribute("style");
        }
        break;
      case "mail":
        if(!input.value.trim().match(EMAIL_REGEX)){
          messages.push("Адрес электронной почты должен быть существующим. Допустимые символы: <strong>буквы английского алфавита, цифры</strong>. Должен присутствовать символ <strong>@</strong>. В части домена также допустимы <strong>тире и точка</strong>.");
          input.style.backgroundColor = "rgb(255,0,0, 0.5)";
        }else{
          input.removeAttribute("style");
        }
        break;
      case "title":
        if(input.value.trim().length < 5){
          messages.push("Тема запроса должна содержать не меньше 5 символов");
          input.style.backgroundColor = "rgb(255,0,0, 0.5)";
        }else{
          input.removeAttribute("style");
        }
        break;
      case "message":
        if(input.value.trim().length < 50){
          messages.push("Краткость, конечно, сестра таланта, но поле сообщения должно содержать не меньше 50 символов (сообщение должно быть осмысленным), а не состоять из 1-2 слов.");
          input.style.backgroundColor = "rgb(255,0,0, 0.5)";
        }else{
          input.removeAttribute("style");
        }
        break;
      default:
        break;
    }
  });
  if(messages.length > 0){
    messages = messages.map(msg => `<li>${msg}</li>`);
    hints.innerHTML = `Обнаружены проблемы<ul>${messages.join("\n")}</ul>`;
    button.setAttribute("disabled","");
  }else{
    let filled = true;
    for(let input of FORM_FIELDS){
      console.log
      if(!input.value.trim()){
        filled = false;
        break;
      }
    }
    if(filled){
      button.removeAttribute("disabled");
    }else{
      button.setAttribute("disabled","");
    }
    hints.innerHTML = DEFAULT_HINT;
  }
}

function formSubmit(event){
  event.preventDefault();
  console.log(event);
  let popup = document.createElement("div");
  popup.classList.add("card");
  popup.classList.add("popup");
  document.querySelector(".gridfields").append(popup);
  let cog = document.createElement("div");
  cog.classList.add("cog");
  popup.append(cog);
  let deg = 0;
  let rotation = setInterval(()=>{
    cog.style.transform = `rotate(${deg}deg)`;
    deg += 10;
  }, 40);
  setTimeout(()=>{
    clearInterval(rotation);
    cog.remove();
    let timer = 5000;
    let updater = setInterval(()=>{
      popup.innerText = `Это учебный проект и обратной связи на данный момент нет.\nНо, спасибо за ваш интерес!\n\nДанное окно закроется через ${timer/1000} секунд`;
      timer -= 100;
    }, 100);
    setTimeout(()=>{
      clearInterval(updater);
      popup.remove();
    }, timer);
  }, 1500);
}

addEventListener("load", event => {
  let hints = document.querySelector("#hints");
  DEFAULT_HINT = hints.innerText;
  FORM_FIELDS = Array.from(document.querySelectorAll("input"));
  FORM_FIELDS.push(document.querySelector("textarea"));
  
  FORM_FIELDS.forEach(input => {
    input.addEventListener("focus", focusHighlight);
  });
  document.querySelector("form").addEventListener("input", validateForm);
  document.querySelector("form").addEventListener("submit", formSubmit);
  addEventListener("click", unfocusInputs);
});