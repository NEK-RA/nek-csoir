let apps;

let listFilters = {
  section: "all",
  platform: [],
  license: "any",
};

const platforms = ["Windows", "Linux", "MacOS"];
const licenses = ["foss", "proprietary", "any"];

/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

function buildListItem(data){
  let theme = localStorage.getItem("theme") ?? "dark";
  let reverse = (theme == "dark") ? "light" : "dark";

  let tagList = [];
  if(data.license == "foss"){
    tagList.push("Открытый исходный код");
  }else{
    tagList.push("Проприетарное ПО")
  }
  for(platform of data.platforms){
    tagList.push(platform);
  }
  tagList = tagList.map(tag => `<span class="card ${reverse}-outline">${tag}</span>`);

  let element = `
  <a href="/info/?app=${data.slug}">
  <div class="card list-item ${theme}er">
    <div class="list-item-head">
      <img src="/res/img/apps/${data.logo}" alt="Логотип ${data.name}" class="list-item-img">
      <div class="list-item-name">${data.name}</div>
    </div>
    <div class="list-item-description">${data.description}</div>
    <div class="list-item-tags">
      ${tagList.join("")}
    </div>
  </div>
  </a>
  `
  return element;
}

/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

function updatePermalink(){
  let permalink = document.querySelector("#permalink");
  let section = `section=${listFilters.section}`;
  let license = `license=${listFilters.license}`;
  let platform = `platfrom=${listFilters.platform.join()}`;
  let getParams = [section, platform, license];
  let link = `${location.origin}${location.pathname}?${getParams.join("&")}`;
  permalink.innerHTML = `{{&emsp;${link}&emsp;}}`;
  permalink.href = link;
}

/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////


function copyPermalink(event){
  event.preventDefault();
  let permalink = document.querySelector("#permalink");
  navigator.clipboard.writeText(permalink.href).then(
    () => {alert("Ссылка скопирована")},
    () => {alert("Копирование в буфер обмена не сработало... Попробуйте еще раз или скопируйте ссылку через контекстное меню.")}
  );
}

/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

function listApps(){
  let appsCopy = apps.slice();
  if(listFilters.section != "all"){
    appsCopy = appsCopy.filter(el => el.section == listFilters.section);
  }
  if(listFilters.license != "any"){
    appsCopy = appsCopy.filter(el => el.license == listFilters.license);
  }
  if(listFilters.platform.length > 0){
    appsCopy = appsCopy.filter(el => {
      for(platform of listFilters.platform){
        if(!el.platforms.includes(platform)){
          return false;
        }
      }
      return true;
    })
  }

  let listCard = document.querySelector("#list");
  let html = [];
  for(app of appsCopy){
    html.push(buildListItem(app));
  }
  listCard.innerHTML = html.join("");
  updatePermalink();
}

/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

async function loadApps(){
  let listCard = document.querySelector("#list");
  let loading = document.createElement("img");
  loading.src = "/res/img/list/loading.svg";
  loading.classList.add("loading");
  loading.classList.add("dark");
  listCard.appendChild(loading);

  let request = await fetch("/res/apps-list.json");
  if(request.status == 200){
    apps = await request.json();
    loading.remove();
    listApps();
  }else{
    let filters = document.querySelector("#filters");
    let errorDiv = document.createElement("div");
    errorDiv.innerText = `Произошла ошибка загрузки списка приложений :(\nВместо корректного результата получена ошибка: ${request.status} - ${request.statusText}`;
    errorDiv.classList.add("card");
    errorDiv.classList.add("warning");
    filters.parentNode.insertBefore(errorDiv, filters);
  }
}

/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

function updateFiltersCard(){
  let sectionZone = Array.from(document.querySelectorAll("#filters option"));
  let index = sectionZone.findIndex(el => el.value == listFilters.section);
  if(index != -1){
    sectionZone[index].setAttribute("selected", "");
  }

  let platformZone = Array.from(document.querySelectorAll("#filters input[type=checkbox]"));
  platformZone.forEach(el => {
    if(listFilters.platform.includes(el.value)){
      el.setAttribute("checked","");
    }
  });

  let licenseZone = Array.from(document.querySelectorAll("#filters input[type=radio]"));
  index = sectionZone.findIndex(el => el.value == listFilters.license);
  if(index != -1){
    sectionZone[option].setAttribute("checked", "");
  }
}

/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

function checkUrl(){
  let getParams = location.search.trim();
  if(getParams){
    // Разбиение GET-параметров
    getParams = getParams.substring(1).split("&");
    // Фильтр раздела
    let sectionParam = getParams.find(param => param.startsWith("section"));
    if(sectionParam){
      if(sectionParam.includes("=") && !sectionParam.endsWith("=")){
        listFilters.section = sectionParam.split("=")[1];
      }
    }
    // Фильтр поддерживаемых платформ
    let platformParam = getParams.find(param => param.startsWith("platform"));
    if(platformParam){
      if(platformParam.includes("=") && !platformParam.endsWith("=")){
        let rawPlaforms = platformParam.split("=")[1].split(",");
        listFilters.platform = rawPlaforms.map(el => el.trim()).filter(el => platforms.includes(el)) ?? [];
      }
    }
    // Фильтр лицензии
    let licenseParam = getParams.find(param => param.startsWith("license"));
    if(licenseParam){
      if(licenseParam.includes("=") && !licenseParam.endsWith("=")){
        let license = licenseParam.split("=")[1];
        if(licenses.includes(license)){
          listFilters.license = license;
        }
      }
    }
  }
  updateFiltersCard();
  listApps();
}

/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

function filterUpdater(event){
  let filterValue;

  if(event.target.localName == "input"){
    filterValue = event.target.value;
    // checkbox = фильтр платформы
    if(event.target.type == "checkbox"){
      if(platforms.includes(filterValue)){
        if(event.target.checked && !listFilters.platform.includes(filterValue)){
          listFilters.platform.push(filterValue);
        }else if(!event.target.checked && listFilters.platform.includes(filterValue)){
          listFilters.platform = listFilters.platform.filter(value => value != filterValue);
        }
      }
    // radio = фильтр лицензии
    }else if(event.target.type == "radio"){
      console.log("radio clicked");
      console.log(filterValue);
      if(licenses.includes(filterValue)){
        console.log("license found");
        if(filterValue != "any"){
          listFilters.license = filterValue;
        }else{
          listFilters.license = "any";
        }
      }
    }
    // обновление списка
    listApps();
    // если не input, а option = фильтр раздела
  }else if(event.target.localName == "option" || event.target.localName == "select"){
    filterValue = event.target.value;
    if(filterValue == "all"){
      listFilters.section = "all";
    }else{
      listFilters.section = event.target.value;
    }
    // обновление списка
    listApps();
  }
}

/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

addEventListener("load", (event) => {
  loadApps().then(()=>{
    checkUrl();
    document.querySelector("#filters").addEventListener("click", filterUpdater);
    document.querySelector("#filters").addEventListener("change", filterUpdater);
    document.querySelector("#permalink").addEventListener("click", copyPermalink);
  });
})