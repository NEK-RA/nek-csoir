let navShown = false;

function changeColors(){
  let darkElements = document.querySelectorAll(".dark");
  let darkerElements = document.querySelectorAll(".darker");
  let darkShadowElements = document.querySelectorAll(".dark-shadow");
  let darkOutlineElements = document.querySelectorAll(".dark-outline");

  let lightElements = document.querySelectorAll(".light");
  let lighterElements = document.querySelectorAll(".lighter");
  let lightShadowElements = document.querySelectorAll(".light-shadow");
  let lightOutlineElements = document.querySelectorAll(".light-outline");

  darkElements.forEach(element =>{
    element.classList.remove("dark");
    element.classList.add("light");
  });
  darkerElements.forEach(element =>{
    element.classList.remove("darker");
    element.classList.add("lighter");
  });
  darkShadowElements.forEach(element => {
    element.classList.remove("dark-shadow");
    element.classList.add("light-shadow");
  });
  darkOutlineElements.forEach(element => {
    element.classList.remove("dark-outline");
    element.classList.add("light-outline");
  });

  lightElements.forEach(element =>{
    element.classList.remove("light");
    element.classList.add("dark");
  });
  lighterElements.forEach(element =>{
    element.classList.remove("lighter");
    element.classList.add("darker");
  });
  lightShadowElements.forEach(element => {
    element.classList.remove("light-shadow");
    element.classList.add("dark-shadow");
  });
  lightOutlineElements.forEach(element => {
    element.classList.remove("light-outline");
    element.classList.add("dark-outline");
  });

  let button = document.querySelector("#theme-switcher");
  if(button.innerText == "nightlight"){
    button.innerText = "brightness_7";
  }else{
    button.innerText = "nightlight";
  }
}

function switchTheme(event){
  changeColors();
  if(localStorage.getItem("theme") == "light"){
    localStorage.setItem("theme", "dark");
  }else{
    localStorage.setItem("theme", "light");
  }
}

function applyCurrentTheme(){
  if(localStorage.getItem("theme") == "light"){
    changeColors();
  }
}

function getComparisonList(){
  return JSON.parse(localStorage.getItem("comparisonList"));
}

function setComparisonList(list){
  localStorage.setItem("comparisonList", JSON.stringify(list));
}

function getComparisonCategory(){
  return localStorage.getItem("comparisonCategory");
}

function setComparisonCategory(category){
  if(category == null){
    localStorage.removeItem("comparisonCategory");
  }else{
    localStorage.setItem("comparisonCategory", category);
  }
}

function switchNavVisibility(event){
  let themeSwitcher = document.querySelector("#theme-switcher");
  if(event.target != themeSwitcher){
    let nav = document.querySelector("nav");
    if(nav){
      navShown = !navShown;
      if(navShown){
        nav.classList.remove("nav-hidden");
        nav.classList.add("nav-shown");
      }else{
        nav.classList.remove("nav-shown");
        nav.classList.add("nav-hidden");
      }
    }
  }
}

addEventListener("load", (event)=>{
  // localStorage.clear();
  document.querySelector("#theme-switcher").addEventListener("click", switchTheme);
  if(localStorage.getItem("theme") == null){
    localStorage.setItem("theme", "dark");
  }
  applyCurrentTheme();
  if(localStorage.getItem("comparisonList") == null){
    setComparisonList([]);
  }
  document.querySelector("header").addEventListener("click", switchNavVisibility);
})